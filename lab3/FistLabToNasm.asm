NULL              EQU 0                         ; Constants
STD_OUTPUT_HANDLE EQU -11

extern _GetStdHandle@4                          ; Import external symbols
extern _WriteFile@20                           ; Windows API functions, decorated
extern _ExitProcess@4

global Start                                    ; Export symbols. The entry point

section .data                                   ; Initialized data segment
 Message        db "Result :", 0Dh, 0Ah
 MessageLength  EQU $-Message                   ; Address of this line ($) - address of Message
 i db 1
 b db '2'

section .bss                                    ; Uninitialized data segment
 StandardHandle resd 1
 Written        resd 1
 res resb 1 
 c resb 1
 d resb 1
 

section .text                                   ; Code segment
Start:

 mov al, '0'
 mov al, [i]
 mov bl, [b]
 mov cl, [c]
 mov dl, [d]
 

; moving the first number to second number to bl
; and subtracting ascii '0' to convert it into a decimal number

 mov bl, '2'
 sub bl, '0'
 sub bl, al
 mov cl, bl
 mov [c], cl
 
 add cl, [b]
 mov dl, cl
 
 add dl, dl
 add dl, [c]
 sub dl, '0'
 mov [res], dl
                                                 ; adding ascii '0' to convert it into a string
 push  STD_OUTPUT_HANDLE
 call  _GetStdHandle@4
 mov   dword [StandardHandle], EAX

	push  NULL                                     ; 5th parameter
 push  Written                                  ; 4th parameter
 push  MessageLength                            ; 3rd parameter
 push  Message                                  ; 2nd parameter
 push  dword [StandardHandle]                   ; 1st parameter
 call  _WriteFile@20                            ; Output can be redirect to a file using >

 push  NULL                                     ; 5th parameter
 push  Written                                  ; 4th parameter
 push  1                            ; 3rd parameter
 push  res                                  ; 2nd parameter
 push  dword [StandardHandle]                   ; 1st parameter
 call  _WriteFile@20                            ; Output can be redirect to a file using >

 push  NULL
 call  _ExitProcess@4