NULL              EQU 0                     ; Constants
STD_OUTPUT_HANDLE EQU -11

extern _GetStdHandle@4              ; Import external symbols
extern _WriteFile@20                    ; Windows API functions, decorated
extern _ExitProcess@4
extern wsprintfA

global Start                                     ; Export symbols. The entry point

section .data                                   ; Initialized data segment
 Message1        db "Result: %d", 0Dh, 0Ah
 MessageLength1  EQU $-Message1                   ; Address of this line ($) - address of Message
 i db 10
 b db 3

section .bss                                    ; Uninitialized data segment
 StandardHandle resd 1
 Written        resd 1
 temp resb 32
 
section .text                                    ; Code segment
Start:

 mov eax, 0
 _l1:
 inc eax,
 add [temp], eax
 cmp eax, 20
 jl _l1
 
 mov eax, [temp]
 
 push	eax
 push	Message1
 push	temp
 call	wsprintfA                          ;insert number into string
 
 push  STD_OUTPUT_HANDLE
 call  _GetStdHandle@4
 mov   dword [StandardHandle], EAX

 push  NULL                                   ; 5th parameter
 push  Written                                 ; 4th parameter
 push  MessageLength1                 ; 3rd parameter
 push  temp                                ; 2nd parameter
 push  dword [StandardHandle]      ; 1st parameter
 call  _WriteFile@20                       ; Output can be redirect to a file using >

 
 push  NULL
 call  _ExitProcess@4
