NULL              EQU 0                         ; Constants
extern _ExitProcess@4
extern Beep
extern Sleep

global Start                                    ; Export symbols. The entry point

section .data
section .bss
section .text                                   ; Code segment
Start:

push  250
push  523
call Beep

push  262
push  523
call Beep

push  300
push  587
call Beep

push  250
push  523
call Beep
	
push  350
push  698
call Beep

push  350
push  659
call Beep

push  250
push  523
call Beep

push  250
push  523
call Beep

push  300
push  587
call Beep

push  250
push  523
call Beep

push  400
push  785
call Beep

push  350
push  698
call Beep

push  250
call Sleep

push  250
push  523
call Beep

push  520
push  523
call Beep

push  450
push  1047
call Beep

push  350
push  880
call Beep

push  350
push  698
call Beep

push  300
push  659
call Beep

push  450
push  587
call Beep

push  450
push  1047
call Beep

push  450
push  1047
call Beep

push  350
push  880
call Beep

push  400
push  698
call Beep

push  350
push  785
call Beep

push  250
push  698
call Beep

 
 push  NULL
 call  _ExitProcess@4
