NULL              EQU 0                         ; Constants
extern _ExitProcess@4
extern Beep
extern Sleep

global Start                                    ; Export symbols. The entry point

section .data
section .bss
section .text                                   ; Code segment
Start:

push  250
push  659
call Beep

push  250
push  659
call Beep

push 500
call Sleep

push  250
push  659
call Beep

push 150
call Sleep
	
push  250
push  523
call Beep

push  500
push  659
call Beep

push  500
push  784
call Beep

push 500
call Sleep

push  500
push  392
call Beep

push 500
call Sleep

push  750
push  523
call Beep

push  250
push  392
call Beep

push 500
call Sleep

push  500
push  392
call Beep

push  250
push  392
call Beep

push  500
push  440
call Beep

push  500
push  494
call Beep

push  250
push  494
call Beep

push  333
push  392
call Beep
push  333
push  659
call Beep
push  333
push  783
call Beep
push  500
push  880
call Beep

push  250
push  698
call Beep

push  250
push  783
call Beep

push 150
call Sleep
push  500
push  659
call Beep

push  250
push  523
call Beep

push  250
push  587
call Beep

push  750
push  494
call Beep

 
 push  NULL
 call  _ExitProcess@4
