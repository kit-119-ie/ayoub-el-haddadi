MB_ICONERROR EQU 0x00000010
NULL              EQU 0                         ; Constants
extern _ExitProcess@4
extern MessageBeep
extern Sleep

global Start                                    ; Export symbols. The entry point

section .data
section .bss
section .text                                   ; Code segment
Start:

push MB_ICONERROR
call MessageBeep

 
 push  NULL
 call  _ExitProcess@4
