title addition of 3 numbers in byte format
include \masm32\include\masm32rt.inc
.data
a1 db 0ffh,0 ;
i db 10 ,0 ;
b db 3 ,0 ;
dres db 0,0 ;
mres db 0,0 ;
titl db "Transforming of second lab to MASM: ",0; simplified window name
st1 dq 1 dup(0),0 ; message output buffer
ifmt db "Displaying variants to calculate formula: ",0ah,
"i = 10", 10, "b = 3",10, "mres = i * b",10,"dres = i / b",10,"Multiplication result =  %d",10,
"Division result = %x",0ah,0ah

.code
entry_point proc
xor ax, ax ; zeroing - so as not to see garbage
xor bx, bx
xor cx, cx
	    mov al, [i]
		cbw         ;(convert byte to word)
		mov bl, [b]

		div bl
		mov cl, al     
		mov [dres],cl  ;dres = i/b

		mov al, [i]
		mov bl, [b]
		mul bl
		mov [mres], al         ;mres = i*b

movzx ax,al ; extension (can be omitted if there is no garbage)
movzx cx,cl 
movzx eax,ax ; extension (can be omitted if there is no garbage)
movzx ecx,cx
invoke wsprintf, ADDR st1, ADDR ifmt, eax, ecx;
invoke MessageBox,0,addr st1,addr titl, MB_OK
invoke ExitProcess,0
entry_point endp
end