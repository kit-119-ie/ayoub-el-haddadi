title addition of 3 numbers in byte format
include \masm32\include\masm32rt.inc
.data
a1 db 0ffh,0 ;
a2 db 255,0 ;
a3 db 255,0 ;
res dd 0,0 ;
titl db "Output via function MessageBox",0; simplified window name
st1 dq 1 dup(0),0 ; message output buffer
ifmt db "Displaying numbers from memory through MessageBox:",0ah,
"a1 = 255d", 10, "a2 = 255d",10, "a3 = 255d",10,
"res = a1 + a2 + a3 = %x",0ah,0ah
.code
entry_point proc
xor eax,eax ; zeroing - so as not to see garbage
mov al,a1 ; loading а1
add al,a2 ; al := a1 + a2
add al,a3 ; al := (a1 + a2) + a3
movzx eax,ax ; extension (can be omitted if there is no garbage)
invoke wsprintf, ADDR st1, ADDR ifmt,eax;
invoke MessageBox,0,addr st1,addr titl, MB_OK
invoke ExitProcess,0
entry_point endp
end
