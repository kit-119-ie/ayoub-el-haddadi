NULL              EQU 0                         ; Constants
STD_OUTPUT_HANDLE EQU -11


global Start

extern _ExitProcess@4
extern printf         ;from msvcrt

section .bss
name:   resb 100

section .data

struc mytype 
 
  .elem1:      resd    1 
  .elem2:      resd    1 
  .elem3:      resd    1 
  .elem4:      resd    1 
 
endstruc

str1:
  istruc mytype
   at mytype.elem1    
     dd 1
   at mytype.elem2
     dd -1
   at mytype.elem3 
     dd -2
   at mytype.elem4
     dd 3
  iend

str2:
  istruc mytype
   at mytype.elem1    
     dd 0
   at mytype.elem2
     dd -2
   at mytype.elem3 
     dd -1
   at mytype.elem4
     dd -3
  iend

titl1 db "Input matrix:",0ah," 1 -1 -2 3 ",0ah," 0 -2 -1 -3",0ah,0

temp db "%d",0ah,0

need_stop db 0

section .text
Start:
push titl1
call    printf
add     esp,8

mov eax, str1
jp get_sum

code1:

mov dword [need_stop], 1

mov eax, str2
jp get_sum
 
get_sum:
mov ecx, 4
mov ebx, 0
sum_minus_values:
		
cmp dword [eax], NULL
jge l1

add ebx, [eax]

l1:

add eax, 4

loop sum_minus_values

push ebx
push temp
call    printf
add     esp,8

cmp dword [need_stop], NULL
je code1
		
 push  NULL
 call  _ExitProcess@4