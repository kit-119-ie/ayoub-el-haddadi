NULL              EQU 0                         ; Constants
STD_OUTPUT_HANDLE EQU -11

global Start

extern _ExitProcess@4
extern printf         ;from msvcrt

section .bss
name:   resb 100

struc   mytype ; struct declaration
 
  .field1:      resd    1 
  .field2:      resw    1 
  .field3:      resb    1 
  .field4:      resb    32 
 
endstruc

section .data

somelabel1: ; creation and initialization of struct instance
  istruc mytype
   at mytype.field1    
     dw 0
   at mytype.field2
     dw 0
   at mytype.field3 
     db    15 
   at mytype.field4
     db    'Hello first structs ',0,0
  iend

somelabe2: ; creation and initialization of struct instance
  istruc mytype
   at mytype.field1    
     dw 0
   at mytype.field2
     dw 0
   at mytype.field3 
     db    15 
   at mytype.field4
     db    'Hello second structs ',0,0
  iend

section .text
Start:
        push somelabel1 + mytype.field4
        call    printf
        add     esp,8

        push somelabe2 + mytype.field4
        call    printf
        add     esp,8

 push  NULL
 call  _ExitProcess@4
