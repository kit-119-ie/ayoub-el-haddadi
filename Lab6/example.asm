NULL              EQU 0                         ; Constants
GENERIC_WRITE              EQU 0x40000000
CREATE_ALWAYS              EQU 2
FILE_ATTRIBUTE_ARCHIVE              EQU 0x80

BSIZE1 EQU 4

extern wsprintfA
extern CreateFileA
extern CloseHandle
extern WriteFile
extern _ExitProcess@4
 
global Start                                    ; Export symbols. The entry point

section .data                                   ; Initialized data segment
 fName           db "File-1m.txt",0
 fmt             db "%d",0, 0 
 Buf1 dq 'test',0, 0 
 len1 equ ($ - Buf1) / 8
 startValue dd 1
 fHandle dd 0
 cMessage dd 0
 
 section .bss 
	
 section .text
 
 Start: 
		push NULL
		push FILE_ATTRIBUTE_ARCHIVE
		push CREATE_ALWAYS
		push NULL
		push NULL
		push GENERIC_WRITE
		push fName
		call CreateFileA
  
		mov [fHandle], eax
		
		push NULL
		push cMessage
		push BSIZE1
		push Buf1
		push eax
		call WriteFile
		
		push fHandle
		call CloseHandle
  
 push  NULL
 call  _ExitProcess@4
