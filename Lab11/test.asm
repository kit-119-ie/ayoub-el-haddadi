include \masm32\include\masm32rt.inc

.MMX
.data ;
titl db "Output via function MessageBox",0; simplified window name
st1 dq 1 dup(0),0 ; message output buffer
ifmt db "Answer %d %d",0
.code
entry_point proc

mov eax, 11
mov ebx, 10

movd mm0, eax

movd mm1, ebx

paddd mm0, mm1 ; 11 + 10

movd eax, mm0
movd ebx, mm1

invoke wsprintf, ADDR st1, ADDR ifmt, eax, ebx;
invoke MessageBox, 0,addr st1,addr titl, MB_OK

invoke ExitProcess, 0

entry_point endp
end
