#include <iostream>


int main() {

	int i = 1, b = 2, c, d, res;

	 __asm {
		mov eax, i
		mov ebx, b
		mov ecx, c
		mov edx, d
		 
		sub ebx, eax       // c = b - i = 1
		mov ecx, ebx
		mov c, ecx

		add ecx, b       //d = c + b = 3
		mov edx, ecx

		add edx, edx     //res = d + d + c = 7
		add edx, c
		mov res, edx

	}

	std::cout << res << std::endl;

}
